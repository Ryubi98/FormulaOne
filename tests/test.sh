#!/bin/sh

separation()
{
    echo ""
    echo "===================="
    echo ""
}

ls tests/maps_testsuite > tests/maps.txt

echo "TESTSUITE FOR FORMULA ONE"

while IFS='\n' read -r maps; do
    separation
    echo $maps
    ./check tests/maps_testsuite/$maps
done < tests/maps.txt

separation

rm tests/maps.txt
