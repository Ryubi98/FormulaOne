#include "dijkstra.h"

static float weight_diago(int orientation, int i)
{
    switch (orientation)
    {
        case 4:
            if (i == 0 || i == 2)
                return 0.3f;
            break;
        case 5:
            if (i == 0 || i == 3)
                return 0.3f;
            break;
        case 6:
            if (i == 1 || i == 2)
                return 0.3f;
            break;
        case 7:
            if (i == 1 || i == 3)
                return 0.3f;
            break;
    }

    return 1.5f;
}

static float weight_edge(int orientation, int i)
{
    switch (orientation)
    {
        case 0:
            if (i == 4 || i == 5)
                return 0.3f;
            break;
        case 1:
            if (i == 6 || i == 7)
                return 0.3f;
            break;    
        case 2:
            if (i == 4 || i == 6)
                return 0.3f;
            break;
        case 3:
            if (i == 5 || i == 7)
                return 0.3f;
            break;
    }

    return 1.5f;
}

static float add_weight_turn(struct graph *current, int i)
{
    if (current->orientation == i)
        return 0.0f;
    if (current->orientation < 4)
        return weight_edge(current->orientation, i);
    else
        return weight_diago(current->orientation, i);
}

static void check_graph(struct queue *queue, struct graph *first)
{
    while (!is_queue_empty(queue))
    {
        struct graph *current = dequeue(queue);
        for (int i = 0; i < 8; i++)
        {
            struct graph *next = current->list[i];
            if (!next)
                continue;

            float friction = 0;
            if (current->ft == GRASS)
                friction = CAR_GRASS_FRICTION_FACTOR;
            else
                friction = CAR_FRICTION_FACTOR;
            float current_weight = current->weight;
            if (i < 4)
                current_weight += (1.0f / friction);
            else
                current_weight += (1.4f / friction);

            if (!next->visited)
            { 
                next->visited = 1;
                enqueue(queue, next);
            }

            current_weight += add_weight_turn(current, i);

            if (next->weight == 0 || next->weight > current_weight)
            {
                if (next != first)
                {
                    next->orientation = i;
                    next->weight = current_weight;
                    next->prev = current;
                }
            }
        }
    }
}

static struct stack *create_path(struct graph *current)
{
    struct stack *stack = init_stack();
    if (!stack)
        return NULL;

    while (current)
    {
        add_stack(stack, current);
        current = current->prev;
    }

    return stack;
}

struct stack *dijkstra(struct graph *graph, int start_index, int finish_index)
{
    struct queue *queue = init_queue();
    if (!queue)
        return NULL;

    struct graph *start = graph + start_index;
    start->visited = 1;
    start->orientation = 0;
    enqueue(queue, start);
    check_graph(queue, start);

    struct graph *finish = graph + finish_index;
    free(queue);
    return create_path(finish);
}

