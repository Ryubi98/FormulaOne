#include <stdio.h>

#include "control.h"
#include "graph.h"
#include "queue.h"
#include "stack.h"
#include "dijkstra.h"

static int check_orientation(struct car *car, int orientation, int i)
{
    float a = car->position.x;
    float b = car->position.y;

    switch (orientation)
    {
    case 0:
        return map_get_floor(car->map, a, b - i) == BLOCK;
    case 1:
        return map_get_floor(car->map, a, b + i) == BLOCK;
    case 2:
        return map_get_floor(car->map, a - i, b) == BLOCK;
    case 3:
        return map_get_floor(car->map, a + i, b) == BLOCK;
    case 4:
        return map_get_floor(car->map, a - i, b - i) == BLOCK;
    case 5:
        return map_get_floor(car->map, a + i, b - i) == BLOCK;
    case 6:
        return map_get_floor(car->map, a - i, b + i) == BLOCK;
    case 7:
        return map_get_floor(car->map, a + i, b + i) == BLOCK;
    }

    return 0;
}

static int check_frontblock(struct car *car, int orientation)
{
    for (size_t i = 1; i < 10; i++)
    {
       if (check_orientation(car, orientation, i))
           return 1;
    }

    return 0;
}

static enum move orientation(struct car *car, float a, float b, int ori)
{
    struct vector2 vector =
    {
        .x = a - car->position.x,
        .y = b - car->position.y
    };

    float car_x = car->direction.x;
    float car_y = car->direction.y;
    float rotate = atan2(vector.y, vector.x) - atan2(car_y, car_x);
 
    if ((rotate < -0.012 && rotate >= -3) || (rotate < 10 && rotate > 3))
        return BRAKE_AND_TURN_LEFT;
    if ((rotate > 0.012 && rotate <= 3) || (rotate < -3 && rotate > -10))
        return BRAKE_AND_TURN_RIGHT;
    else
    {
        float limit = CAR_MAX_SPEED / 3.8;
        if (fabsf(car->speed.y) <= limit && fabsf(car->speed.x) <= limit)
            return ACCELERATE;
        else
        {
            if ((abs(car_x) == 0 && abs(car_y) == 1)
                    || (abs(car_x) == 1 && abs(car_y) == 0))
            {
                float limit2 = CAR_MAX_SPEED / 2.8;
                if (check_frontblock(car, ori) != 1 && fabsf(car->speed.y)
                        <= limit2 && fabsf(car->speed.x) <= limit2)
                    return ACCELERATE;
                else
                    return BRAKE;
            }
            else
                return BRAKE;
        }
    }
}

enum move update(struct car *car)
{
    static struct stack *path = NULL;
    static struct vector2 v =
    {
        0
    };
    static struct graph *g = NULL;
    static struct graph *current = NULL;

    if (!path)
    {
        struct map *map = car->map;
        int index_start = (int)map->start.x + map->width * (int)map->start.y;
        int index_finish = 0;

        g = create_graph(map, &index_finish);
        path = dijkstra(g, index_start, index_finish);
        current = pop_stack(path);

        v.x = current->coord.x;
        v.y = current->coord.y;
    }

    if (v.x - 1 <= car->position.x && v.x + 1 >= car->position.x &&
            v.y - 1 <= car->position.y && v.y + 1 >= car->position.y)
    {
        if (!is_stack_empty(path))
            current = pop_stack(path);
        v.x = current->coord.x;
        v.y = current->coord.y;
    }

    return orientation(car, current->coord.x, current->coord.y, current->orientation);
}
