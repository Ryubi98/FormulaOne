#include "graph.h"

static void add_diago(struct graph *g, int height, int width)
{
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            struct graph *left = g[j + width * i].list[2];
            struct graph *right = g[j + width * i].list[3];
            if (!left && !right)
                continue;

            struct graph *up = g[j + width * i].list[0];
            if (up)
            {
                if (left && g[(j - 1) + width * (i - 1)].ft != BLOCK)
                    g[j + width * i].list[4] = g + (j - 1) + width * (i - 1);
                if (right && g[(j + 1) + width * (i - 1)].ft != BLOCK)
                    g[j + width * i].list[5] = g + (j + 1) + width * (i - 1);
            }

            struct graph *down = g[j + width * i].list[1];
            if (down)
            {
                if (left && g[(j - 1) + width * (i + 1)].ft != BLOCK)
                    g[j + width * i].list[6] = g + (j - 1) + width * (i + 1);
                if (right && g[(j + 1) + width * (i + 1)].ft != BLOCK)
                    g[j + width * i].list[7] = g + (j + 1) + width * (i + 1);
            }
        }
    }
}

struct graph *create_graph(struct map *map, int *index_finish)
{
    int size = map->width * map->height;
    struct graph *g = calloc(size, sizeof(*g));

    for (int i = 0; i < map->height; i++)
    {
        for (int j = 0; j < map->width; j++)
        {
            g[j + map->width * i].coord.x = j + 0.5f;
            g[j + map->width * i].coord.y = i + 0.5f;

            g[j + map->width * i].ft = map->floor[j + map->width * i];
            if (g[j + map->width * i].ft == FINISH)
                *index_finish = j + map->width * i;

            for (int k = 0; k < 8; k++)
                g[j + map->width * i].list[k] = NULL;

            if (i != 0 && map->floor[j + map->width * (i - 1)] != BLOCK)
                g[j + map->width * i].list[0] = g + j + map->width * (i - 1);
            if (i != map->height - 1 &&
                    map->floor[j + map->width * (i + 1)] != BLOCK)
                g[j + map->width * i].list[1] = g + j + map->width * (i + 1);

            if (j != 0 && map->floor[(j - 1) + map->width * i] != BLOCK)
                g[j + map->width * i].list[2] = g + (j - 1) + map->width * i;
            if (j != map->width - 1 &&
                    map->floor[(j + 1) + map->width * i] != BLOCK)
                g[j + map->width * i].list[3] = g + (j + 1) + map->width * i;
        }
    }

    add_diago(g, map->height, map->width);

    return g;
}

