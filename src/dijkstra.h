#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "graph.h"
#include "queue.h"
#include "stack.h"

struct stack *dijkstra(struct graph *graph, int start_index, int finish_index);

#endif /* ! DIJKSTRA_H */
