#ifndef QUEUE_H
#define QUEUE_H

#include <stdlib.h>

#include "graph.h"

struct queue
{
    struct node_queue *first;
    struct node_queue *last;
};

struct node_queue
{
    struct graph *graph;
    struct node_queue *next;
};

struct queue *init_queue(void);
int is_queue_empty(struct queue *q);
void enqueue(struct queue *q, struct graph *g);
struct graph *dequeue(struct queue *q);

#endif /* ! QUEUE_H */
