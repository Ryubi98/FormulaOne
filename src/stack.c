#include "stack.h"

struct stack *init_stack(void)
{
    return calloc(1, sizeof(struct stack));
}

int is_stack_empty(struct stack *stack)
{
    if (!stack)
        return 1;
    return stack->first == NULL;
}

void add_stack(struct stack *stack, struct graph *graph)
{
    if (!stack)
        return;

    struct node_stack *new = calloc(1, sizeof(*new));
    new->graph = graph;
    new->next = stack->first;
    stack->first = new;
}

struct graph *pop_stack(struct stack *stack)
{
    if (!stack || !stack->first)
        return NULL;

    struct node_stack *ele = stack->first;
    stack->first = ele->next;

    struct graph *graph = ele->graph;
    free(ele);

    return graph;
}
