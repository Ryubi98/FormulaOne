#ifndef GRAPH_H
#define GRAPH_H

#include <stdlib.h>

#include "control.h"

struct graph
{
    int visited;
    float weight;
    struct graph *prev;
    enum floortype ft;
    struct vector2 coord;
    int orientation;
    int is_path;

    struct graph *list[8];
    //list[0] -> up
    //list[1] -> down
    //list[2] -> left
    //list[3] -> right
    //list[4] -> up_left
    //list[5] -> up_right
    //list[6] -> down_left
    //list[7] -> down_right
};

struct graph *create_graph(struct map *map, int *index_finish);

#endif /* ! GRAPH_H */
