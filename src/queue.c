#include "queue.h"

struct queue *init_queue(void)
{
    return calloc(1, sizeof(struct queue));
}

int is_queue_empty(struct queue *q)
{
    if (!q)
        return 1;
    return q->first == NULL;
}

void enqueue(struct queue *q, struct graph *g)
{
    if (!q)
        return;

    struct node_queue *new = calloc(1, sizeof(*new));
    if (!new)
        return;

    new->graph = g;
    if (!q->first)
        q->first = new;
    else
        q->last->next = new;
    q->last = new;
}

struct graph *dequeue(struct queue *q)
{
    if (!q || !q->first)
        return NULL;

    struct node_queue *ele = q->first;
    if (q->first == q->last)
    {
        q->first = NULL;
        q->last = NULL;
    }
    else
        q->first = ele->next;

    struct graph *graph = ele->graph;
    free(ele);

    return graph;
}
