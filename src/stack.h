#ifndef STACK_H
#define STACK_H

#include <stdlib.h>

#include "graph.h"

struct stack
{
    struct node_stack *first;
};

struct node_stack
{
    struct graph *graph;
    struct node_stack *next;
};

struct stack *init_stack(void);
int is_stack_empty(struct stack *stack);
void add_stack(struct stack *stack, struct graph *graph);
struct graph *pop_stack(struct stack *stack);

#endif /* ! STACK_H */
